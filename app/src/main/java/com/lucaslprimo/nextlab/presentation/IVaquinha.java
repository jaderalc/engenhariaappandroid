package com.lucaslprimo.nextlab.presentation;

import io.reactivex.Observable;

public interface IVaquinha {

    Observable<Vaquinha> getVaquinha();
    Observable<Boolean> withdraw();
}
