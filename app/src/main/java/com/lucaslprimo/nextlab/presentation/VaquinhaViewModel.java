package com.lucaslprimo.nextlab.presentation;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.lucaslprimo.nextlab.business.Model;

public class VaquinhaViewModel extends ViewModel {

    MutableLiveData<Vaquinha> vaquinha = new MutableLiveData();
    IVaquinha vaquinhaImpl;

    public VaquinhaViewModel() {
        vaquinhaImpl = Model.getInstance();
    }

    public void getVaquinha(){
        vaquinhaImpl.getVaquinha()
                .subscribe(result -> {
                    vaquinha.setValue(result);
                },error -> error.printStackTrace());
    }

    public void withdraw() {
        vaquinhaImpl.withdraw().subscribe();
    }
}
