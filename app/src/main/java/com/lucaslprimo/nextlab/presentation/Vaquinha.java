package com.lucaslprimo.nextlab.presentation;

public class Vaquinha {

    public Double actualValue;
    public Double objectiveValue;
    public boolean isClose;
    public boolean canWithdraw;

    public Vaquinha(double actualValue, double objectiveValue, boolean isClose, boolean canWithdraw) {
        this.actualValue = actualValue;
        this.objectiveValue = objectiveValue;
        this.isClose = isClose;
        this.canWithdraw = canWithdraw;
    }
}
