package com.lucaslprimo.nextlab.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.lucaslprimo.nextlab.R;

public class MainActivity extends AppCompatActivity {

    TextView txtValue, txtObjective;
    ProgressBar progressBar;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtValue = findViewById(R.id.txt_value);
        txtObjective = findViewById(R.id.txt_objective);
        progressBar = findViewById(R.id.progressBar);
        button = findViewById(R.id.btn_action);

        VaquinhaViewModel vaquinhaViewModel = ViewModelProviders.of(this).get(VaquinhaViewModel.class);

        vaquinhaViewModel.vaquinha.observe(this, this::updateViews);

        vaquinhaViewModel.getVaquinha();

        button.setOnClickListener(view -> {
            vaquinhaViewModel.withdraw();

            Vaquinha vaquinha = vaquinhaViewModel.vaquinha.getValue();

            if(vaquinha.actualValue == vaquinha.objectiveValue) {
                startActivity(new Intent(MainActivity.this, WithdrewActivity.class));
            }else {
                startActivity(new Intent(MainActivity.this, WithdrewBonusActivity.class));
            }
        });
    }

    private void updateViews(Vaquinha vaquinha) {
        txtValue.setText(String.format("R$ %.2f",vaquinha.actualValue));
        txtObjective.setText(String.format("R$ %.2f",vaquinha.objectiveValue));
        progressBar.setProgress((int)getPercentage(vaquinha.actualValue,vaquinha.objectiveValue));
        button.setEnabled(vaquinha.canWithdraw);
        updateProgressBarColor(vaquinha.isClose);
    }

    private void updateProgressBarColor(Boolean isClose) {
        if(isClose){
            progressBar.setBackgroundColor(getResources().getColor(R.color.above_color));
        }else{
            progressBar.setBackgroundColor(getResources().getColor(R.color.below_color));
        }
    }

    public double getPercentage(double x,double y){
        return (x/y)*100;
    }
}
