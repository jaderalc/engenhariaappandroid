package com.lucaslprimo.nextlab.business;

import com.lucaslprimo.nextlab.data.IRepository;
import com.lucaslprimo.nextlab.data.RepositoryApi;
import com.lucaslprimo.nextlab.data.VaquinhaResponse;
import com.lucaslprimo.nextlab.presentation.IVaquinha;
import com.lucaslprimo.nextlab.presentation.Vaquinha;

import io.reactivex.Observable;

public class Model implements IVaquinha {

    IRepository repository = RepositoryApi.getInstance();

    private VaquinhaResponse vaquinhaResponse;
    private static Model instance;

    public static Model getInstance() {
        if(instance == null){
            instance = new Model();
        }

        return instance;
    }

    @Override
    public Observable<Vaquinha> getVaquinha() {
        return repository.getVaquinha().map(vaquinhaResponse -> {
            Vaquinha vaquinha = new Vaquinha(vaquinhaResponse.balance,
                    vaquinhaResponse.amount_contracted,
                    isCloseToTheObjective(vaquinhaResponse),
                            canWithdraw(vaquinhaResponse));

            this.vaquinhaResponse = vaquinhaResponse;

            return vaquinha;
        });
    }

    @Override
    public Observable<Boolean> withdraw() {
        return repository.postWithdraw(vaquinhaResponse.id).map( withdrawResponse -> true);
    }

    private boolean isCloseToTheObjective(VaquinhaResponse vaquinhaResponse) {
        return getPercentage(vaquinhaResponse.balance,vaquinhaResponse.amount_contracted) > 50;
    }

    private boolean  canWithdraw(VaquinhaResponse vaquinhaResponse) {
        return vaquinhaResponse.balance >= vaquinhaResponse.amount_contracted;
    }

    private int getPercentage(double x, double y){
        return (int)(x/y)*100;
    }
}
