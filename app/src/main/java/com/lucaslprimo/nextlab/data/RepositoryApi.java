package com.lucaslprimo.nextlab.data;

import com.lucaslprimo.nextlab.presentation.Vaquinha;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepositoryApi implements IRepository{

    private static RepositoryApi instance;
    private static final String API_BASE_URL = "https://jsonplaceholder.typicode.com/";
    private RetrofitService retrofitService;

    public static RepositoryApi getInstance() {
        if(instance== null){
            instance = new RepositoryApi();
        }
        return instance;
    }

    private RepositoryApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        retrofitService = retrofit.create(RetrofitService.class);
    }

    public Observable<VaquinhaResponse> getVaquinha(){
        return retrofitService.getVaquinha();
    }

    @Override
    public Observable<WithdrawResponse> postWithdraw(int id) {
        return retrofitService.postWithdraw(id);
    }
}
