package com.lucaslprimo.nextlab.data;

import java.io.Serializable;

public class VaquinhaResponse implements Serializable{

    public int id;
    public double balance;
    public double amount_contracted;
}
