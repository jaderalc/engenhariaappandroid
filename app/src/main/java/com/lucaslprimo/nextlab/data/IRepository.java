package com.lucaslprimo.nextlab.data;


import io.reactivex.Observable;

public interface IRepository {
    Observable<VaquinhaResponse> getVaquinha();
    Observable<WithdrawResponse> postWithdraw(int id);
}
