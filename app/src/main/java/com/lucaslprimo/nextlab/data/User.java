package com.lucaslprimo.nextlab.data;

import java.io.Serializable;

public class User implements Serializable {

    public int userId;
    public int id;
    public String title;
    public boolean completed;
}
