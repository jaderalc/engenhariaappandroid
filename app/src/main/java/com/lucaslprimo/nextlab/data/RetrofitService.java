package com.lucaslprimo.nextlab.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RetrofitService {

  @GET("vaquinha")
  Observable<VaquinhaResponse> getVaquinha();

  @GET("withdraw/{id}")
  Observable<WithdrawResponse> postWithdraw(@Path("id") int id);
}