## Challenge - Integration
Duration: 30

### Swagger - API Doc
Backend team released the first version of Swagger

GET vaquinha's datas
Response
```json
{
	"id" : "int",
	"balance" : "double",
	"amount_contracted" : "double"
}
```

POST withdraw/{id}
Response
```json
{
	"amount_withdraw" : "double",
	"amount_contracted" : "double"
}
```

It's up to you again. Develop some lines of code to consume this Api just released.
Remember to do it on a new branch and open a pull request after you finish.